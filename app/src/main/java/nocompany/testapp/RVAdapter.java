package nocompany.testapp;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder>{

    private ItemClickListener clickListener;
    private List<City> cities;

    public class PersonViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        CardView cv;
        TextView cityName;
        TextView cityFoundation;
        NetworkImageView cityImage;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            cityName = (TextView)itemView.findViewById(R.id.city_name);
            cityFoundation = (TextView)itemView.findViewById(R.id.city_foundation);
            cityImage = (NetworkImageView)itemView.findViewById(R.id.city_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }



    RVAdapter(List<City> cities){
        this.cities = cities;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, final int i) {
        personViewHolder.cityName.setText(cities.get(i).name);
        personViewHolder.cityFoundation.setText(cities.get(i).foundation);
        personViewHolder.cityImage.setImageUrl(cities.get(i).image, VolleySingleton.getInstance().getImageLoader());
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

}
