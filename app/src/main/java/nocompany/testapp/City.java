package nocompany.testapp;

public class City {
    String name;
    String foundation;
    String image;
    String coordinates;

    City(String name, String foundation, String image, String coordinates) {
        this.name = name;
        this.foundation = foundation;
        this.image = image;
        this.coordinates = coordinates;
    }
}
