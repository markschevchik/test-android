package nocompany.testapp;

import java.util.List;

public interface NetworkJSONLoaderDoneListener {
    void onGetResponse(List<City> citiesArray);
}
