package nocompany.testapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by elMarco on 04.02.2017.
 */

public class PrefSingleton {
    private static PrefSingleton sharePref = new PrefSingleton();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static final String stateOneFile = "S1";
    private static final String stateTwoFile = "S2";

    public static PrefSingleton getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return sharePref;
    }

    public void saveFirstState(Boolean state) {
        editor.putBoolean(stateOneFile, state);
        editor.commit();
    }

    public Boolean getFirstState() {
        return sharedPreferences.getBoolean(stateOneFile, false);
    }

    public void saveSecondState(String state) {
        editor.putString(stateTwoFile, state);
        editor.commit();
    }

    public String getSecondState() {
        return sharedPreferences.getString(stateTwoFile, "");
    }

    public void clearAll() {
        editor.clear();
        editor.commit();
    }

}
