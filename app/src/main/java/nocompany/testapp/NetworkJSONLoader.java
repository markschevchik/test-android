package nocompany.testapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import java.util.ArrayList;
import java.util.List;

public class NetworkJSONLoader {
    private Context context;
    private RecyclerView list_cities;
    private List<City> cities = new ArrayList<>();
    private NetworkJSONLoaderDoneListener response_listener;

    public NetworkJSONLoader(Context con, RecyclerView rv, NetworkJSONLoaderDoneListener response_listener) {
        context = con;
        list_cities = rv;
        this.response_listener = response_listener;
    }

    public void requestJSON(String url) {
        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        /*final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getResources().getString(R.string.loading));
        pDialog.show();*/

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET, url,
                null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("NetworkJSONLoader resp", response.toString());

                try {
                    JSONArray citiesArray = response.getJSONArray("data");

                    //NetworkJSONLoader.this.cities = new ArrayList<>();
                    for (int i=0; i<citiesArray.length();i++){
                        NetworkJSONLoader.this.cities.add(new City(citiesArray.getJSONObject(i).getString("city_name"), citiesArray.getJSONObject(i).getString("foundation"),
                                "https://woodar.ru/Android/" + citiesArray.getJSONObject(i).getString("image_name") + ".png", citiesArray.getJSONObject(i).getString("coordinates")));
                    }

                    response_listener.onGetResponse(cities);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

                //pDialog.hide();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //pDialog.hide();
            }
        });

        // Adding request to request queue
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }
}
