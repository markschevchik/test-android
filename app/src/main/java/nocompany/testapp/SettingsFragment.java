package nocompany.testapp;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ToggleButton;

/**
 * Created by elMarco on 04.02.2017.
 */

public class SettingsFragment extends DialogFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private EditText secondStateEditText;
    private ToggleButton toggleButton;
    private Button closeButton;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.settings_title);
        View v = inflater.inflate(R.layout.fragment_settings, container);

        PrefSingleton ps = new PrefSingleton();


        secondStateEditText = (EditText) v.findViewById(R.id.secondStateEditText);
        toggleButton = (ToggleButton) v.findViewById(R.id.toggleButton);
        closeButton = (Button) v.findViewById(R.id.btnCancel);

        toggleButton.setChecked(PrefSingleton.getInstance(getActivity().getApplicationContext()).getFirstState());
        //toggleButton.setChecked(ps.getFirstState());
        toggleButton.setOnCheckedChangeListener(this);

        secondStateEditText.setText(PrefSingleton.getInstance(getActivity().getApplicationContext()).getSecondState());
        //secondStateEditText.setText(ps.getSecondState());

        closeButton.setOnClickListener(this);


        return v;
    }

    @Override
    public void onClick(View v) {
        PrefSingleton.getInstance(getActivity().getApplicationContext()).saveSecondState(secondStateEditText.getText().toString());
        PrefSingleton.getInstance(getActivity().getApplicationContext()).saveFirstState(toggleButton.isChecked());
        dismiss();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //TODO: implement something
    }
}
